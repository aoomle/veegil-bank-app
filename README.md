Name: Abdulmalik Muhammad
email: abdulmmbs@gmail.com

**Veegil Bank** is a beautiful mobile banking app that’s gives you real time access to your accounts,.It is built with iOS design guidelines in mind. It is simple enough to pick up and use immediately, and powerful  enough to tweak it just the way you want. The delightful interface is coupled with extensive features that feel the part on your device.

Demo of the app
https://gitlab.com/aoomle/veegil-bank-app/-/raw/main/Demo.mp4

**FEATURES:**

- Transfer to accounts in Veegil Bank
- Transfer to accounts in other banks in Nigeria
- View the balance on your bank accounts
- Manage your account and preview your transaction history 
- Dark mode and high-contrast backgrounds
- Accessibility labels, dynamic font sizing
- Universal app( iPhone, iPad and Mac)



**HOW TO RUN THE APP:**

- You must have a Macbook and Xcode installed

- Open the Veegil Bank.xcodeproj 

- Run the play button on the app tab
