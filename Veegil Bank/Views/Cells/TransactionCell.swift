//
//  TransactionCell.swift
//  Veegil Bank
//
//  Created by Abdulmalik Muhammad on 09/05/2021.
//

import UIKit

class TransactionCell: UITableViewCell {
  static let reusedID = "SettingsCell"
  let settingsOption = UILabel()
  
  
   var titleCell = UILabel()
   var moneyTransaction = UILabel()
   var dateLabelCell = UILabel()
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
 
    setupViews()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  fileprivate func setupViews() {
    backgroundColor = General.Font.backgroundColor
    
    titleCell.textColor = General.Font.tableTitleColor
    moneyTransaction.textColor = General.Font.tableTransactionColor
    dateLabelCell.textColor = General.Font.tableSubtitleColor
    
    titleCell.font = .preferredFont(forTextStyle: .body)
    dateLabelCell.font = .preferredFont(forTextStyle: .caption2)
    addViews()
  }
  
  
  fileprivate func addViews() {
    addSubview(titleCell)
    addSubview(moneyTransaction)
    addSubview(dateLabelCell)
    
    titleCell.translatesAutoresizingMaskIntoConstraints = false
    moneyTransaction.translatesAutoresizingMaskIntoConstraints = false
    dateLabelCell.translatesAutoresizingMaskIntoConstraints = false
    
    setupLayout()
    
  }

  fileprivate func setupLayout() {
    
    NSLayoutConstraint.activate([
      
      titleCell.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
      titleCell.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
      titleCell.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10),
      titleCell.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -20),
      
      moneyTransaction.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
//      moneyTransaction.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
      moneyTransaction.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10),
      moneyTransaction.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -20),
      
      dateLabelCell.topAnchor.constraint(equalTo: titleCell.safeAreaLayoutGuide.bottomAnchor),
      dateLabelCell.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
      dateLabelCell.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10),
    ])
  }
}
