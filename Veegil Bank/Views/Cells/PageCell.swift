//
//  PageCell.swift
//  Veegil Bank
//
//  Created by Abdulmalik Muhammad on 07/05/2021.
//

import UIKit

class PageCell: UICollectionViewCell {
  
  var page: Page? {
    didSet {
      guard  let page = page else { return }
      imageView.image = UIImage(named: page.image)
      
      let attributedText = NSMutableAttributedString(string: "\(page.title)", attributes: [NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: .headline),
                                                                                         
                                                                                         NSAttributedString.Key.foregroundColor : General.Font.bigFontColor!
      ])
      
      attributedText.append(NSMutableAttributedString(string: "\n\n\(page.subheadline)", attributes: [NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: .subheadline),
                                                                                                                                                                           NSAttributedString.Key.foregroundColor : General.Font.mediumFontColor!]))
      
      descriptionText.attributedText = attributedText
      descriptionText.textAlignment = .center
    }
  }
  

  fileprivate let imageView = UIImageView(image: #imageLiteral(resourceName: "imageSavings"))
  fileprivate let descriptionText = UITextView()
  
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupView()
    setupLayout()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  //This is where i setup all the UI elements
  fileprivate func setupView(){
    backgroundColor = General.Font.backgroundColor
    
    
    let attributedText = NSMutableAttributedString(string: "Veegil Bank", attributes: [NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: .title1),
                                                                                       
                                                                                       NSAttributedString.Key.foregroundColor : General.Font.bigFontColor!
    ])
    
    attributedText.append(NSMutableAttributedString(string: "\n\nIs a modern and reliable mobile banking app that simplifies your life and saves you time", attributes: [NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: .subheadline),
                                                                                                                                                                         NSAttributedString.Key.foregroundColor : General.Font.mediumFontColor!]))
    descriptionText.attributedText = attributedText
    descriptionText.textAlignment = .center
    descriptionText.isEditable = false
    descriptionText.isScrollEnabled = false
    descriptionText.backgroundColor = General.Font.backgroundColor
  }
  

  //Set up layout for UI elements
  fileprivate func setupLayout(){
    let topContainer = UIView()
    addSubview(topContainer)
    topContainer.addSubview(imageView)
    addSubview(descriptionText)
    
    topContainer.translatesAutoresizingMaskIntoConstraints = false
    imageView.translatesAutoresizingMaskIntoConstraints = false
    descriptionText.translatesAutoresizingMaskIntoConstraints = false
    
    NSLayoutConstraint.activate([
      
      topContainer.topAnchor.constraint(equalTo: topAnchor),
      topContainer.leadingAnchor.constraint(equalTo: leadingAnchor),
      topContainer.trailingAnchor.constraint(equalTo: trailingAnchor),
      topContainer.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.5),
      
      imageView.centerXAnchor.constraint(equalTo: topContainer.centerXAnchor),
      imageView.centerYAnchor.constraint(equalTo: topContainer.centerYAnchor),
      imageView.heightAnchor.constraint(equalTo: topContainer.heightAnchor, multiplier: 0.5),
      imageView.widthAnchor.constraint(equalTo: topContainer.widthAnchor, multiplier: 0.5),
      
      descriptionText.topAnchor.constraint(equalTo: topContainer.bottomAnchor),
      descriptionText.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
      descriptionText.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
    ])
  }
}
