//
//  TransactionController.swift
//  Veegil Bank
//
//  Created by Abdulmalik Muhammad on 08/05/2021.
//

import UIKit

class TransactionController: UIViewController {
  
  fileprivate let topContainer = UIView()

  fileprivate let tableView = UITableView()
  
  fileprivate let segmentedView = UISegmentedControl(items: ["Deposit", "Withdraw"])
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = General.Font.backgroundColor
    navigationController?.setNavigationBarHidden(true, animated: false)
    addViews()
    setupViews()
    configureTableView()
    setupLayout()
  }
  
  fileprivate func addViews() {
    view.addSubview(segmentedView)
    segmentedView.translatesAutoresizingMaskIntoConstraints = false
   
    view.addSubview(tableView)
    tableView.translatesAutoresizingMaskIntoConstraints = false
    
  }
  
  fileprivate func setupViews() {
    topContainer.backgroundColor = General.Font.banner
    segmentedView.backgroundColor = General.Font.textFieldColor
    segmentedView.selectedSegmentTintColor = General.Font.buttonColor
    segmentedView.selectedSegmentIndex = 0
    segmentedView.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.white] , for: UIControl.State.normal)
    
  }
  
  fileprivate func setupLayout() {
    addViews()
    NSLayoutConstraint.activate([
      segmentedView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
      segmentedView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40),
      segmentedView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40),

      tableView.topAnchor.constraint(equalTo: segmentedView.bottomAnchor, constant: 20),
      tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
      tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
      tableView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.8),
    ])
  }
  
  func configureTableView() {
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.isScrollEnabled = true
    tableView.delegate = self
    tableView.dataSource = self
    tableView.register(HomeCell.self, forCellReuseIdentifier: "cell")
  }
  
  
}

extension TransactionController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return 20
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HomeCell
    cell.titleCell.text = "Abdulmalik Muhammad"
    cell.dateLabelCell.text = "Sat 8 May"
    cell.moneyTransaction.text = "-₦1000"
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 80
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: false)
  }
}
