//
//  TransactionMenus.swift
//  Veegil Bank
//
//  Created by Abdulmalik Muhammad on 09/05/2021.
//

import UIKit

struct TransactionMenuList {
  var name: String
  var image: UIImage
}

class TransactionMenus: UIViewController {
  
  fileprivate var largeTitle = UILabel()
  fileprivate var tableView = UITableView()
  fileprivate var cancelButton = UIButton(type: .system)
  
  fileprivate let menuList = [TransactionMenuList(name: "Transfer", image: #imageLiteral(resourceName: "transferButton")),
                              TransactionMenuList(name: "Withdraw", image: #imageLiteral(resourceName: "withdrawButton")),
                              TransactionMenuList(name: "Transaction", image: #imageLiteral(resourceName: "transactionButton"))
  ]
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = General.Font.backgroundColor
    setupViews()
    cancelButton.addTarget(self, action: #selector(handleCancelButton), for: .touchUpInside)
  }
  
  @objc fileprivate func handleCancelButton() {
    dismiss(animated: true, completion: nil)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    navigationController?.setNavigationBarHidden(true, animated: true)
  }
  
  fileprivate func setupViews() {
    largeTitle.text = "What do you want\nto do?"
    largeTitle.numberOfLines = 2
    largeTitle.font = .preferredFont(forTextStyle: .largeTitle)
    
    cancelButton.setTitle("Cancel", for: .normal)
    cancelButton.setTitleColor(UIColor.white, for: .normal)
    cancelButton.backgroundColor = General.Font.buttonColor
    cancelButton.layer.cornerRadius = 24
    addViews()
  }
  
  fileprivate func addViews() {
    view.addSubview(largeTitle)
    largeTitle.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(tableView)
    tableView.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(cancelButton)
    cancelButton.translatesAutoresizingMaskIntoConstraints = false
    
    configureTableView()
    setupLayout()
  }
  
  func configureTableView() {
    tableView.backgroundColor = General.Font.backgroundColor
    tableView.isScrollEnabled = false
    tableView.delegate = self
    tableView.dataSource = self
    tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellID")
  }
  
  fileprivate func setupLayout() {
    
    NSLayoutConstraint.activate([
      largeTitle.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      largeTitle.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40),
      largeTitle.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40),
      largeTitle.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.3),
      
      tableView.topAnchor.constraint(equalTo: largeTitle.safeAreaLayoutGuide.bottomAnchor),
      tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40),
      tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40),
      tableView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.4),
      
      cancelButton.topAnchor.constraint(equalTo: tableView.safeAreaLayoutGuide.bottomAnchor),
      cancelButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40),
      cancelButton.widthAnchor.constraint(equalToConstant: 120),
      cancelButton.heightAnchor.constraint(equalToConstant: 65),
    ])
  }
}


extension TransactionMenus: UITableViewDelegate, UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return menuList.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath)
    let menu = menuList[indexPath.row]
    cell.backgroundColor = General.Font.backgroundColor
    cell.textLabel?.text = menu.name
    cell.imageView?.image = menu.image
    cell.accessoryType = .disclosureIndicator
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 100
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: false)
   
    let transfer = overFullScreen(viewController: TransferController())
    let withdraw = overFullScreen(viewController: WithdrawController())
    let transaction = overFullScreen(viewController: TransferController())
    
    switch indexPath.row {
    case 0: present(transfer, animated: true, completion: nil)
    case 1: present(withdraw, animated: true, completion: nil)
    case 2: self.navigationController?.pushViewController(transaction, animated: true)
    default:
      print("Default")
      break
    }
  }
  
  fileprivate func overFullScreen(viewController: UIViewController) -> UIViewController {
    let present = viewController
    present.modalPresentationStyle = .overCurrentContext
    return present
  }
  
}
