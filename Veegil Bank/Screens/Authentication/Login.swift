//
//  Welcome.swift
//  Veegil Bank
//
//  Created by Abdulmalik Muhammad on 07/05/2021.
//

import UIKit

class Login: UIViewController {
  
  fileprivate let descriptionText = UITextView()
  fileprivate let loginButton = UIButton(type: .system)
  fileprivate let copyRightText = UILabel()
  fileprivate let phoneNumberTextField = UITextField()
  fileprivate let passwordTextField = UITextField()
  
  var isPhoneNumberEntered: Bool {
    return !phoneNumberTextField.text!.isEmpty
  }
  
  var isPasswordEntered: Bool {
    return !passwordTextField.text!.isEmpty
  }
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupView()
    setupLayout()
    navigationController?.setNavigationBarHidden(false, animated: true)
    navigationController?.navigationBar.prefersLargeTitles = true
    actionButton()
    
  }
  
  //This is where i setup all the UI elements
  fileprivate func setupView(){
    view.backgroundColor = General.Font.backgroundColor
    
    
    let attributedText = NSMutableAttributedString(string: "LOG IN", attributes: [NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: .title1),
                                                                                  
                                                                                  NSAttributedString.Key.foregroundColor : General.Font.bigFontColor!
    ])
    
    attributedText.append(NSMutableAttributedString(string: "\n\nProvide your phone number and password in order to sign to your account", attributes: [NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: .subheadline),
                                                                                                                                                        NSAttributedString.Key.foregroundColor : General.Font.mediumFontColor!]))
    descriptionText.attributedText = attributedText
    descriptionText.textAlignment = .center
    descriptionText.isEditable = false
    descriptionText.isScrollEnabled = false
    descriptionText.backgroundColor = General.Font.backgroundColor
    
    
    phoneNumberTextField.placeholder = "Phone number"
    phoneNumberTextField.textAlignment = .center
    phoneNumberTextField.backgroundColor = General.Font.textFieldColor
    phoneNumberTextField.layer.cornerRadius = 15
    phoneNumberTextField.keyboardType = .phonePad
    
    passwordTextField.placeholder = "Password"
    passwordTextField.textAlignment = .center
    passwordTextField.backgroundColor = General.Font.textFieldColor
    passwordTextField.layer.cornerRadius = 15
    passwordTextField.isSecureTextEntry = true
    
    loginButton.backgroundColor = General.Font.buttonColor
    loginButton.setTitle("Log in", for: .normal)
    loginButton.setTitleColor(UIColor.white, for: .normal)
    loginButton.layer.cornerRadius = 10
    
    copyRightText.text = "© 2021 Veegil Bank, Inc All right reserved"
    copyRightText.textAlignment = .center
    copyRightText.textColor = General.Font.mediumFontColor
    copyRightText.font = .preferredFont(forTextStyle: .footnote)
    
    passwordTextField.resignFirstResponder()
    
  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    view.endEditing(true)
  }
  
  //Set up layout for UI elements
  fileprivate func setupLayout(){
    
    let topContainer = UIView()
    view.addSubview(topContainer)
    topContainer.addSubview(descriptionText)
    view.addSubview(descriptionText)
    view.addSubview(loginButton)
    view.addSubview(phoneNumberTextField)
    view.addSubview(passwordTextField)
    view.addSubview(copyRightText)
    
    topContainer.translatesAutoresizingMaskIntoConstraints = false
    descriptionText.translatesAutoresizingMaskIntoConstraints = false
    loginButton.translatesAutoresizingMaskIntoConstraints = false
    phoneNumberTextField.translatesAutoresizingMaskIntoConstraints = false
    passwordTextField.translatesAutoresizingMaskIntoConstraints = false
    
    copyRightText.translatesAutoresizingMaskIntoConstraints = false
    
    NSLayoutConstraint.activate([
      
      topContainer.topAnchor.constraint(equalTo: view.topAnchor),
      topContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      topContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor),
      topContainer.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5),
      
      descriptionText.topAnchor.constraint(equalTo: topContainer.safeAreaLayoutGuide.topAnchor),
      descriptionText.leadingAnchor.constraint(equalTo: topContainer.leadingAnchor, constant: 20),
      descriptionText.trailingAnchor.constraint(equalTo: topContainer.trailingAnchor, constant: -20),
      descriptionText.centerXAnchor.constraint(equalTo: topContainer.centerXAnchor),
      
      
      phoneNumberTextField.topAnchor.constraint(equalTo: descriptionText.bottomAnchor, constant: 20),
      phoneNumberTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
      phoneNumberTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
      phoneNumberTextField.heightAnchor.constraint(equalToConstant: 50),
      
      passwordTextField.topAnchor.constraint(equalTo: phoneNumberTextField.bottomAnchor, constant: 20),
      passwordTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
      passwordTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
      passwordTextField.heightAnchor.constraint(equalToConstant: 50),
      
      loginButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 20),
      loginButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
      loginButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
      loginButton.heightAnchor.constraint(equalToConstant: 50),
      
      
      copyRightText.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
      copyRightText.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
      copyRightText.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
      copyRightText.centerXAnchor.constraint(equalTo: view.centerXAnchor)
      
    ])
  }
  
  fileprivate func actionButton() {
    loginButton.addTarget(self, action: #selector(signInAction), for: .touchUpInside)
    
  }
  
  @objc fileprivate func signInAction() {
    view.endEditing(true)
    guard isPhoneNumberEntered, let phoneNumber = phoneNumberTextField.text else {
      alertView(title: "Something went wrong", message: "Phone number can't be empty, please type your phone number")
      return
    }
    
    guard isPasswordEntered, let password = passwordTextField.text else {
      alertView(title: "Something went wrong", message: "Password can't be empty")
      return
    }
    
    showLoadingView()
    NetworkAPI.shared.loginUser(phoneNumber: phoneNumber, password: password) { (users, errorMessage) in
      self.dismissLoadingView()
      guard let users = users else {
        DispatchQueue.main.async {
          self.alertView(title: "Something went wrong", message: errorMessage!.description)
        }
        return
      }
      
//      print(users["phoneNumber"])
      DispatchQueue.main.async {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.pushViewController(HomeControllerModern(), animated: true)
      }
      
    }
  }
  
  fileprivate func alertView(title: String, message: String) {
    let alertview = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let actionView = UIAlertAction(title: "Ok", style: .default, handler: nil)
    alertview.addAction(actionView)
    present(alertview, animated: true, completion: nil)
  }
}
