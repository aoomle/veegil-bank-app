//
//  Welcome.swift
//  Veegil Bank
//
//  Created by Abdulmalik Muhammad on 07/05/2021.
//

import UIKit

class Welcome: UIViewController {
  
  fileprivate let imageView = UIImageView(image: #imageLiteral(resourceName: "imageLogo"))
  fileprivate let descriptionText = UITextView()
  fileprivate let loginButton = UIButton(type: .system)
  fileprivate let openAccountButton = UIButton(type: .system)
  fileprivate let copyRightText = UILabel()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationController?.setNavigationBarHidden(true, animated: true)
    navigationItem.setHidesBackButton(true, animated: false)
    setupView()
    setupLayout()
    actionButtons()
  }
  
  //This is where i setup all the UI elements
  fileprivate func setupView(){
    view.backgroundColor = General.Font.backgroundColor
  
    
    let attributedText = NSMutableAttributedString(string: General.Page.titleThree, attributes: [NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: .title1),
                                                                                       
                                                                                       NSAttributedString.Key.foregroundColor : General.Font.bigFontColor!
    ])
    
    attributedText.append(NSMutableAttributedString(string: "\n\n\(General.Page.subheadlineThree)", attributes: [NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: .subheadline),
                                                                                                                                                                         NSAttributedString.Key.foregroundColor : General.Font.mediumFontColor!]))
    descriptionText.attributedText = attributedText
    descriptionText.textAlignment = .center
    descriptionText.isEditable = false
    descriptionText.isScrollEnabled = false
    descriptionText.backgroundColor = General.Font.backgroundColor
    
    loginButton.backgroundColor = General.Font.buttonColor
    loginButton.setTitle("Log in", for: .normal)
    loginButton.setTitleColor(UIColor.white, for: .normal)
    loginButton.layer.cornerRadius = 10
    
    openAccountButton.setTitle("Open new account", for: .normal)
    openAccountButton.setTitleColor(General.Font.bigFontColor, for: .normal)
  
    
    copyRightText.text = "© 2021 Veegil Bank, Inc All right reserved"
    copyRightText.textAlignment = .center
    copyRightText.textColor = General.Font.mediumFontColor
    copyRightText.font = .preferredFont(forTextStyle: .footnote)
  }
  
  //Set up layout for UI elements
  fileprivate func setupLayout(){
    
    let topContainer = UIView()
    view.addSubview(topContainer)
    topContainer.addSubview(imageView)
    view.addSubview(descriptionText)
    view.addSubview(loginButton)
    view.addSubview(openAccountButton)
    view.addSubview(copyRightText)
  
    topContainer.translatesAutoresizingMaskIntoConstraints = false
    imageView.translatesAutoresizingMaskIntoConstraints = false
    descriptionText.translatesAutoresizingMaskIntoConstraints = false
    loginButton.translatesAutoresizingMaskIntoConstraints = false
    openAccountButton.translatesAutoresizingMaskIntoConstraints = false
    copyRightText.translatesAutoresizingMaskIntoConstraints = false
    
    NSLayoutConstraint.activate([
      
      topContainer.topAnchor.constraint(equalTo: view.topAnchor),
      topContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      topContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor),
      topContainer.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5),
    
      imageView.centerXAnchor.constraint(equalTo: topContainer.centerXAnchor),
      imageView.centerYAnchor.constraint(equalTo: topContainer.centerYAnchor),
      imageView.heightAnchor.constraint(equalTo: topContainer.heightAnchor, multiplier: 0.5),
      imageView.widthAnchor.constraint(equalTo: topContainer.widthAnchor, multiplier: 0.7),
      
      descriptionText.topAnchor.constraint(equalTo: topContainer.safeAreaLayoutGuide.bottomAnchor, constant: -60),
      descriptionText.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
      descriptionText.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
      
      loginButton.topAnchor.constraint(equalTo: descriptionText.bottomAnchor, constant: 20),
      loginButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
      loginButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
      loginButton.heightAnchor.constraint(equalToConstant: 50),
      
      openAccountButton.topAnchor.constraint(equalTo: loginButton.bottomAnchor, constant: 20),
      openAccountButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      openAccountButton.heightAnchor.constraint(equalToConstant: 50),

      copyRightText.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
      copyRightText.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
      copyRightText.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
      copyRightText.centerXAnchor.constraint(equalTo: view.centerXAnchor)
    ])
  }
  
  fileprivate func actionButtons() {
    loginButton.addTarget(self, action: #selector(signInAction), for: .touchUpInside)
    openAccountButton.addTarget(self, action: #selector(signUpAction), for: .touchUpInside)
  }
  
  @objc fileprivate func signInAction() {
    pushToAnotherScreen(title: "Sign up", viewController: Login())
  }
  
  @objc fileprivate func signUpAction() {
    pushToAnotherScreen(title: "Log in", viewController: SignUP())
  }
  
  fileprivate func pushToAnotherScreen(title: String, viewController: UIViewController) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: title, style: .plain, target: nil, action: nil)
    navigationController?.pushViewController(viewController, animated: true)
  }
}
