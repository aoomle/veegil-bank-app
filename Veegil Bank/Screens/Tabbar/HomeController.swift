//
//  HomeController.swift
//  Veegil Bank
//
//  Created by Abdulmalik Muhammad on 08/05/2021.
//

import UIKit

class HomeController: UIViewController {
  
  fileprivate let topContainer = UIView()
  fileprivate let dateLabel = UILabel()
  fileprivate let typeAccountLabel = UILabel()
  fileprivate let stackView = UIStackView()
  fileprivate let accountDetails = UITextView()
  fileprivate let transferButton = UIButton(type: .system)
  fileprivate let withdrawButton = UIButton(type: .system)
  fileprivate let transactionButton = UIButton(type: .system)
  
  fileprivate let todayLabel = UILabel()
  
  fileprivate let boxWelcome = UIView()
  fileprivate let welcomeMessage = UILabel()
  
  
  fileprivate let tableView = UITableView()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = General.Font.backgroundColor
    navigationController?.setNavigationBarHidden(true, animated: false)
    addViews()
    setupViews()
    configureTableView()
    setupLayout()
  }
  
  fileprivate func addViews() {
    view.addSubview(topContainer)
    topContainer.translatesAutoresizingMaskIntoConstraints = false
    
    topContainer.addSubview(dateLabel)
    dateLabel.translatesAutoresizingMaskIntoConstraints = false
    topContainer.addSubview(accountDetails)
    accountDetails.translatesAutoresizingMaskIntoConstraints = false
    
    topContainer.addSubview(stackView)
    stackView.translatesAutoresizingMaskIntoConstraints = false
    
    view.addSubview(boxWelcome)
    boxWelcome.translatesAutoresizingMaskIntoConstraints = false
    
    boxWelcome.addSubview(welcomeMessage)
    welcomeMessage.translatesAutoresizingMaskIntoConstraints = false
    
    view.addSubview(tableView)
    tableView.translatesAutoresizingMaskIntoConstraints = false
    
    view.addSubview(todayLabel)
    todayLabel.translatesAutoresizingMaskIntoConstraints = false
  }
  
  fileprivate func setupViews() {
    topContainer.backgroundColor = General.Font.banner
    
    //Views
    dateLabel.text = "Sat 09 May"
    dateLabel.font = .preferredFont(forTextStyle: .footnote)
    dateLabel.textColor = UIColor.white
    
    
    let attributedText = NSMutableAttributedString(string: "Savings", attributes: [NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: .caption2),
                                                                                   NSAttributedString.Key.foregroundColor : UIColor.white])
    
    attributedText.append(NSMutableAttributedString(string: "\n₦200000", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 50),
                                                                                     NSAttributedString.Key.foregroundColor : UIColor.white ]))
    
    attributedText.append(NSMutableAttributedString(string: "\n+2348146191761", attributes: [NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: .caption1),
                                                                                      NSAttributedString.Key.foregroundColor : UIColor.white]))
    
    accountDetails.attributedText = attributedText
    accountDetails.textAlignment = .center
    accountDetails.isEditable = false
    accountDetails.isScrollEnabled = false
    accountDetails.backgroundColor = .clear
    

    transferButton.setTitle("Transfer", for: .normal)
    withdrawButton.setTitle("Withdraw", for: .normal)
    transactionButton.setTitle("Transaction", for: .normal)
    
    transferButton.setTitleColor(UIColor.white, for: .normal)
    withdrawButton.setTitleColor(UIColor.white, for: .normal)
    transactionButton.setTitleColor(UIColor.white, for: .normal)
    
    stackView.addArrangedSubview(transferButton)
    stackView.addArrangedSubview(withdrawButton)
    stackView.addArrangedSubview(transactionButton)
    
    stackView.distribution = .fillEqually
    
    
    
    boxWelcome.backgroundColor = General.Font.textFieldColor
    boxWelcome.layer.cornerRadius = 15
    
    welcomeMessage.text = "Welcome to your Veegil account"
    welcomeMessage.font = .preferredFont(forTextStyle: .title3)
    welcomeMessage.textAlignment = .center
    welcomeMessage.textColor = General.Font.mediumFontColor
    
    todayLabel.text = "Today"
    todayLabel.font = .preferredFont(forTextStyle: .subheadline)
    todayLabel.textColor = General.Font.tableTitleColor
    
  }
  
  fileprivate func setupLayout() {
    addViews()
    NSLayoutConstraint.activate([
      topContainer.topAnchor.constraint(equalTo: view.topAnchor),
      topContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      topContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor),
      topContainer.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.3),
      
      dateLabel.topAnchor.constraint(equalTo: topContainer.safeAreaLayoutGuide.topAnchor, constant: 10),
      dateLabel.leadingAnchor.constraint(equalTo: topContainer.leadingAnchor, constant: 20),
      dateLabel.trailingAnchor.constraint(equalTo: topContainer.trailingAnchor, constant: -20),
      dateLabel.centerXAnchor.constraint(equalTo: topContainer.centerXAnchor),
      
      accountDetails.topAnchor.constraint(equalTo: topContainer.safeAreaLayoutGuide.topAnchor, constant: 30),
      accountDetails.leadingAnchor.constraint(equalTo: topContainer.leadingAnchor),
      accountDetails.trailingAnchor.constraint(equalTo: topContainer.trailingAnchor),
      
      stackView.topAnchor.constraint(equalTo: accountDetails.safeAreaLayoutGuide.bottomAnchor),
      stackView.leadingAnchor.constraint(equalTo: topContainer.leadingAnchor, constant: 20),
      stackView.trailingAnchor.constraint(equalTo: topContainer.trailingAnchor, constant: -20),
      stackView.heightAnchor.constraint(equalTo: topContainer.heightAnchor, multiplier: 0.2),
      
      boxWelcome.topAnchor.constraint(equalTo: topContainer.bottomAnchor, constant: 20),
      boxWelcome.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40),
      boxWelcome.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40),
      boxWelcome.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.1),
      
      welcomeMessage.centerXAnchor.constraint(equalTo: boxWelcome.centerXAnchor),
      welcomeMessage.centerYAnchor.constraint(equalTo: boxWelcome.centerYAnchor),
      
      tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20),
      tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
      tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
      tableView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.4),
      
      todayLabel.bottomAnchor.constraint(equalTo: tableView.topAnchor, constant: -20),
      todayLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
      todayLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
    ])
  }
  
  func configureTableView() {
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.isScrollEnabled = false
    tableView.delegate = self
    tableView.dataSource = self
    tableView.register(HomeCell.self, forCellReuseIdentifier: "cell")
  }
  
  
}

extension HomeController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return 6
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HomeCell
    cell.titleCell.text = "Abdulmalik Muhammad"
    cell.dateLabelCell.text = "Sat 8 May"
    cell.moneyTransaction.text = "-₦1000"
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 80
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: false)
  }
}
