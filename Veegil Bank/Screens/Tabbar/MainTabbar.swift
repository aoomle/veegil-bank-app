//
//  ViewController.swift
//  Veegil Bank
//
//  Created by Abdulmalik Muhammad on 06/05/2021.
//

import UIKit
//import SwiftUI

class MainTabbar: UITabBarController {

  override func viewDidLoad() {
    super.viewDidLoad()
    self.tabBar.tintColor = .label
    viewControllers = [
      generateController(view: HomeController(), title: "", tabBarIcon: "house"),
      generateController(view: TransferController(), title: "", tabBarIcon: "paperplane"),
      generateControllerWithCustomImage(view: WithdrawController(), title: "", image: "withdraw"),
      generateController(view: TransferController(), title: "", tabBarIcon: "chart.bar"),
      generateController(view: SettingsController(), title: "", tabBarIcon: "gear")
    ]
  }
  
  fileprivate func generateController(view controller: UIViewController, title: String, tabBarIcon: UITabBarItem.SystemItem) -> UINavigationController{
    let navController = UINavigationController(rootViewController: controller)
    navController.title = title
    navController.tabBarItem = UITabBarItem(tabBarSystemItem: tabBarIcon, tag: 0)
    navController.tabBarItem.title = title
    return navController
  }
  
  fileprivate func generateController(view controller: UIViewController, title: String, tabBarIcon: String) -> UINavigationController{
    let navController = UINavigationController(rootViewController: controller)
    navController.title = title
    navController.tabBarItem = UITabBarItem(title: title, image: UIImage(systemName: tabBarIcon), tag: 0)
    navController.tabBarItem.title = title
    return navController
  }
  
  fileprivate func generateControllerWithCustomImage(view controller: UIViewController, title: String, image: String) -> UINavigationController{
    let navController = UINavigationController(rootViewController: controller)
    navController.title = title
    navController.tabBarItem = UITabBarItem(title: title, image: UIImage(named: image), tag: 0)
    navController.tabBarItem.title = title
    return navController
  }
}
