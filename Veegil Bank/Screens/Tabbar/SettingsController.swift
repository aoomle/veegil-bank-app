//
//  SettingsController.swift
//  Veegil Bank
//
//  Created by Abdulmalik Muhammad on 08/05/2021.
//

import UIKit


class SettingsController: UIViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .systemOrange
    navigationController?.setNavigationBarHidden(true, animated: false)
  }
  
}
