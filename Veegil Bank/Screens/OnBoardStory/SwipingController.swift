//
//  SwipingController.swift
//  Veegil Bank
//
//  Created by Abdulmalik Muhammad on 07/05/2021.
//

import UIKit

class SwipingController: UICollectionViewController {
  
  let pages = [Page(image: General.Page.imageSavings, title: General.Page.titleOne, subheadline: General.Page.subheadlineOne),
               Page(image: General.Page.securityLogo, title: General.Page.titleTwo, subheadline: General.Page.subheadlineTwo)]
  
  fileprivate let skipButton = UIButton(type: .system)
  fileprivate let startButton = UIButton(type: .system)
  fileprivate let pageController = UIPageControl()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    collectionView.backgroundColor = General.Font.backgroundColor
    collectionView.register(PageCell.self, forCellWithReuseIdentifier: General.cellID)
    collectionView.isPagingEnabled = true
    collectionView.showsHorizontalScrollIndicator = false
    navigationController?.setNavigationBarHidden(true, animated: false)
//    navigationController?.navigationBar.isHidden = true
    collectionView.clipsToBounds = true
    bottomControllers()
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }
  
  
  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return pages.count
  }
  
  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: General.cellID, for: indexPath) as! PageCell
    let page = pages[indexPath.row]
    cell.page = page
    return cell
  }
  
  
  //Show page indicator
  fileprivate func bottomControllers() {
    
    skipButton.setTitle("Skip", for: .normal)
    skipButton.setTitleColor(General.Font.mediumFontColor, for: .normal)
    skipButton.addTarget(self, action: #selector(handleSkip), for: .touchUpInside)
    
    startButton.backgroundColor = General.Font.buttonColor
    startButton.setTitle("Enter Here", for: .normal)
    startButton.setTitleColor(UIColor.white, for: .normal)
    startButton.layer.cornerRadius = 10
    
    startButton.addTarget(self, action: #selector(handleSkip), for: .touchUpInside)
    
    pageController.currentPage = 0
    pageController.numberOfPages = pages.count
    pageController.pageIndicatorTintColor = General.Font.mediumFontColor
    pageController.currentPageIndicatorTintColor = General.Font.buttonColor
    
    view.addSubview(skipButton)
    view.addSubview(startButton)
    view.addSubview(pageController)
    
    startButton.translatesAutoresizingMaskIntoConstraints = false
    skipButton.translatesAutoresizingMaskIntoConstraints = false
    pageController.translatesAutoresizingMaskIntoConstraints = false
    
    NSLayoutConstraint.activate([
      skipButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      skipButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 10),
      skipButton.widthAnchor.constraint(equalToConstant: 40),
      skipButton.heightAnchor.constraint(equalToConstant: 40),
      
      pageController.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
      pageController.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      pageController.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
      pageController.heightAnchor.constraint(equalToConstant: 50),
      
      startButton.bottomAnchor.constraint(equalTo: pageController.safeAreaLayoutGuide.topAnchor, constant: -20),
      startButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      startButton.widthAnchor.constraint(equalToConstant: 100),
      startButton.heightAnchor.constraint(equalToConstant: 50),
    ])
  }
  
  @objc fileprivate func handleSkip() {
    navigationController?.pushViewController(Welcome(), animated: true)
  }
  
}

extension SwipingController: UICollectionViewDelegateFlowLayout {
  
  //cell will to give full size of a controller
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: view.frame.width, height: view.frame.height - 100)
  }
  
  
  //This method will detect number of swipe
  //So I took the advantage of that to change page number
  override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
    let x = targetContentOffset.pointee.x
    pageController.currentPage = Int(x / view.frame.width)
  }
  
}
