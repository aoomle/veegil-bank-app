//
//  NetworkAPI.swift
//  Veegil Bank
//
//  Created by Abdulmalik Muhammad on 07/05/2021.
//

import UIKit

class NetworkAPI {
  static let shared   = NetworkAPI()
  private let baseURL = "https://bank.veegil.com/"
  let cache           = NSCache<NSString, UIImage>()
  
  private init() {}
  
  
  func signUser(phoneNumber: String, password: String, completed: @escaping ([String : Any]?, String?) -> Void) {
    
    //declare parameter as a dictionary which contains string as key and value combination.
    let parameters = ["phoneNumber": phoneNumber, "password": password]
    
    let endpoint = baseURL + "auth/signup?"
    
    //create the url with NSURL
    let newURL = URL(string: endpoint)!
    
    //now create the Request object using the url object
    var request = URLRequest(url: newURL)
    request.httpMethod = "POST" //set http method as POST
    
    do {
      request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to data object and set it as request body
    } catch let error {
      print(error.localizedDescription)
      completed(nil, error.localizedDescription)
    }
    
    //HTTP Headers
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    
    //create dataTask using the session object to send data to the server
    let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
      
      if let _ = error {
        completed(nil, "Unable to complete your request. Please check internet connection")
        return
      }
      
      guard let response = response as? HTTPURLResponse, response.statusCode == 200  else {
        completed(nil, "Invalid response from the server. Please try again")
        return
      }
      
      guard let data = data else {
        completed(nil, "The date received from the server is invalid. Please try again.")
        return
      }
      
      do {
        guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] else {
          completed(nil, NSError(domain: "invalidJSONTypeError", code: -100009, userInfo: nil) as? String)
          print("problem from received ")
          return
        }
        if let userFromJson = json["data"] as? [[String: Any]] {
          for userJson in userFromJson {
            print(userJson)
          }
        }
        completed(json, nil)
      } catch {
        completed(nil, "The date received from the server is invalid. Please try again. from Do catch")
      }
      
    })
    
    task.resume()
  }
  
  func loginUser(phoneNumber: String, password: String, completed: @escaping ([String : Any]?, String?) -> Void) {
    
    //declare parameter as a dictionary which contains string as key and value combination.
    let parameters = ["phoneNumber": phoneNumber, "password": password]
    
    let endpoint = baseURL + "auth/login?"
    
    //create the url with NSURL
    let newURL = URL(string: endpoint)!
    
    //now create the Request object using the url object
    var request = URLRequest(url: newURL)
    request.httpMethod = "POST" //set http method as POST
    
    do {
      request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to data object and set it as request body
    } catch let error {
      print(error.localizedDescription)
      completed(nil, error.localizedDescription)
    }
    
    //HTTP Headers
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    
    //create dataTask using the session object to send data to the server
    let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
      
      if let _ = error {
        completed(nil, "Unable to complete your request. Please check internet connection")
        return
      }
      
      guard let response = response as? HTTPURLResponse, response.statusCode == 200  else {
        completed(nil, "Invalid response from the server. Please try again")
        return
      }
      
      guard let data = data else {
        completed(nil, "The date received from the server is invalid. Please try again.")
        return
      }
      
      do {
        guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] else {
          completed(nil, "Problem from server received")
          return
        }
        
        if let userFromJson = json["data"] as? [[String: Any]] {
          for userJson in userFromJson {
            print(userJson)
          }
        }
        completed(json, nil)
      } catch {
        completed(nil, "The date received from the server is invalid. Please try again. from Do catch")
      }
      
    })
    
    task.resume()
  }
  

  func transfer(phoneNumber: String, amount: String, completed: @escaping ([String : Any]?, String?) -> Void) {
    
    //declare parameter as a dictionary which contains string as key and value combination.
    let parameters = ["phoneNumber": phoneNumber, "amount": amount]
    
    let endpoint = baseURL + "accounts/transfer?"
    
    //create the url with NSURL
    let newURL = URL(string: endpoint)!
    
    //now create the Request object using the url object
    var request = URLRequest(url: newURL)
    request.httpMethod = "POST" //set http method as POST
    
    do {
      request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to data object and set it as request body
    } catch let error {
      print(error.localizedDescription)
      completed(nil, error.localizedDescription)
    }
    
    //HTTP Headers
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    
    //create dataTask using the session object to send data to the server
    let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
      
      if let _ = error {
        completed(nil, "Unable to complete your request. Please check internet connection")
        return
      }
      
      guard let response = response as? HTTPURLResponse, response.statusCode == 200  else {
        completed(nil, "Invalid response from the server. Please try again")
        return
      }
      
      guard let data = data else {
        completed(nil, "The date received from the server is invalid. Please try again.")
        return
      }
      
      do {
        guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] else {
          completed(nil, NSError(domain: "invalidJSONTypeError", code: -100009, userInfo: nil) as? String)
          print("problem from received ")
          return
        }
        completed(json, nil)
      } catch {
        completed(nil, "The date received from the server is invalid. Please try again. from Do catch")
      }
      
    })
    
    task.resume()
  }
  
  func withdraw(phoneNumber: String, amount: String, completed: @escaping ([String : Any]?, String?) -> Void) {
    
    //declare parameter as a dictionary which contains string as key and value combination.
    let parameters = ["phoneNumber": phoneNumber, "amount": amount, "balance": ""]
    
    let endpoint = baseURL + "accounts/withdraw?"
    
    //create the url with NSURL
    let newURL = URL(string: endpoint)!
    
    //now create the Request object using the url object
    var request = URLRequest(url: newURL)
    request.httpMethod = "POST" //set http method as POST
    
    do {
      request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to data object and set it as request body
    } catch let error {
      print(error.localizedDescription)
      completed(nil, error.localizedDescription)
    }
    
    //HTTP Headers
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    
    //create dataTask using the session object to send data to the server
    let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
      
      if let _ = error {
        completed(nil, "Unable to complete your request. Please check internet connection")
        return
      }
      
      guard let response = response as? HTTPURLResponse, response.statusCode == 200  else {
        completed(nil, "Invalid response from the server. Please try again")
        return
      }
      
      guard let data = data else {
        completed(nil, "The date received from the server is invalid. Please try again.")
        return
      }
      
      do {
        guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] else {
          completed(nil, NSError(domain: "invalidJSONTypeError", code: -100009, userInfo: nil) as? String)
          completed(nil, "Problem from server received")
//          print("problem from received ")
          return
        }
        completed(json, nil)
      } catch {
        completed(nil, "The date received from the server is invalid. Please try again. from Do catch")
      }
      
    })
    
    task.resume()
  }
}
