//
//  Constants.swift
//  Veegil Bank
//
//  Created by Abdulmalik Muhammad on 07/05/2021.
//

import UIKit

enum General {
  
  static var cellID = "cellID"
 
  enum Page {
    static var imageLogo = "imageLogo"
    static var imageSavings = "imageSavings"
    static var securityLogo = "securityLogo"
    
    static var titleOne = "Veegil Bank"
    static var titleTwo = "Security & trust"
    static var titleThree = "Safe money management"
    
    static var subheadlineOne = "Is a modern and reliable mobile banking app that simplifies your life and saves you time"
    static var subheadlineTwo = "Protect your finances with an encrypted system."
    static var subheadlineThree = "Nobody likes to wait, so skip queues and create bank account using your phone"
  }
  
  enum Images {
    static var imageLogo = UIImageView(image: #imageLiteral(resourceName: "logo"))
    static var imageSavings = UIImageView(image: #imageLiteral(resourceName: "savings"))
    static var imageSecurity = UIImageView(image: #imageLiteral(resourceName: "security"))

  }
  
  enum Font {
    static var backgroundColor = UIColor(named: "BackgroundColor")
    static var bigFontColor = UIColor(named: "BigFontColor")
    static var mediumFontColor = UIColor(named: "MediumFontColor")
    static var buttonColor = UIColor(named: "ButtonColor")
    static var textFieldColor = UIColor(named: "TextFieldColor")
    static var banner = UIColor(named: "BannerTOP")
    static var tableTitleColor = UIColor(named: "tableTitleColor")
    static var tableSubtitleColor = UIColor(named: "tableSubtitleColor")
    static var tableTransactionColor = UIColor(named: "tableTransactionColor")
  }
  
  enum ScreenSize {
    static let width        = UIScreen.main.bounds.size.width
    static let height       = UIScreen.main.bounds.size.height
    static let maxLength    = max(ScreenSize.width, ScreenSize.height)
    static let minLength    = min(ScreenSize.width, ScreenSize.height)
  }
  
  
  enum DeviceTypes {
    static let idiom                    = UIDevice.current.userInterfaceIdiom
    static let nativeScale              = UIScreen.main.nativeScale
    static let scale                    = UIScreen.main.scale
    
    static let isiPhoneSE               = idiom == .phone && ScreenSize.maxLength == 568.0
    static let isiPhone8Standard        = idiom == .phone && ScreenSize.maxLength == 667.0 && nativeScale == scale
    static let isiPhone8Zoomed          = idiom == .phone && ScreenSize.maxLength == 667.0 && nativeScale > scale
    static let isiPhone8PlusStandard    = idiom == .phone && ScreenSize.maxLength == 736.0
    static let isiPhone8PlusZoomed      = idiom == .phone && ScreenSize.maxLength == 736.0 && nativeScale < scale
    static let isiPhoneX                = idiom == .phone && ScreenSize.maxLength == 812.0
    static let isiPhoneXsMaxAndXr       = idiom == .phone && ScreenSize.maxLength == 896.0
    static let isiPad                   = idiom == .pad && ScreenSize.maxLength >= 1024.0
    
    static func isiPhoneXAspectRatio() -> Bool {
      return isiPhoneX || isiPhoneXsMaxAndXr
    }
  }
  
}
