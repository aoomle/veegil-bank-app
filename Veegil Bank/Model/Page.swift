//
//  Page.swift
//  Veegil Bank
//
//  Created by Abdulmalik Muhammad on 07/05/2021.
//

import Foundation

struct Page {
  let image: String
  let title: String
  let subheadline: String
}
