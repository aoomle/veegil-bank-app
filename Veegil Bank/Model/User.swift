//
//  User.swift
//  Veegil Bank
//
//  Created by Abdulmalik Muhammad on 07/05/2021.
//

import Foundation

class User: NSObject {
  let phoneNumber: String? = nil
  let password: String? = nil
  let balance: String? = nil
}
